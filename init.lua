-- dofile(minetest.get_modpath("spears").."/defaults.lua")

-- local input = io.open(minetest.get_modpath("spears").."/spears.conf", "r")
-- if input then
--	dofile(minetest.get_modpath("spears").."/spears.conf")
--	input:close()
--	input = nil
-- end

dofile(minetest.get_modpath("spears").."/functions.lua")

dofile(minetest.get_modpath("spears").."/tools.lua")

minetest.log("action", "[MOD] Spears loaded with throwing speed " .. minetest.settings:get("spears_throw_speed") .. " and drag coeff. " .. minetest.settings:get("spears_drag_coeff"))
