local S = minetest.get_translator("spears")

function spears_register_spear(spear_type, desc, base_damage, toughness, material)

	minetest.register_tool("spears:spear_" .. spear_type, {
		description = desc,
                wield_image = "spears_spear_" .. spear_type .. ".png^[transform4",
		inventory_image = "spears_spear_" .. spear_type .. ".png",
		wield_scale= {x = 1.5, y = 1.5, z = 1.5},
		on_secondary_use = function(itemstack, user, pointed_thing)
			spears_throw(itemstack, user, pointed_thing)
			if not minetest.settings:get_bool("creative_mode") then
				itemstack:take_item()
			end
			return itemstack
		end,
		on_place = function(itemstack, user, pointed_thing)
			spears_throw(itemstack, user, pointed_thing)
			if not minetest.settings:get_bool("creative_mode") then
				itemstack:take_item()
			end
			return itemstack
		end,
		tool_capabilities = {
			full_punch_interval = 1.5,
			max_drop_level=1,
			groupcaps={
				cracky = {times={[3]=2}, uses=toughness, maxlevel=1},
			},
			damage_groups = {fleshy=base_damage},
		},
		sound = {breaks = "default_tool_breaks"},
		groups = {flammable = 1}
	})
	
	local SPEAR_ENTITY = spears_set_entity(spear_type, base_damage, toughness)
	
	minetest.register_entity("spears:spear_" .. spear_type .. "_entity", SPEAR_ENTITY)
	
	minetest.register_craft({
		output = 'spears:spear_' .. spear_type,
		recipe = {
			{"", "", material},
			{"", "group:stick", ""},
			{"group:stick", "", ""}
		}
	})
	
	minetest.register_craft({
		output = 'spears:spear_' .. spear_type,
		recipe = {
			{material, "", ""},
			{"", "group:stick", ""},
			{"", "", "group:stick"}
		}
	})
end

if minetest.settings:get_bool('spears_enable_stone_spear') then
	spears_register_spear('stone', S('Stone Spear'), 4, 20, 'group:stone')
end

if minetest.get_modpath("pigiron") then
	if minetest.settings:get_bool('spears_enable_iron_spear') then
		spears_register_spear('iron', S('Iron Spear'), 5.5, 30, 'pigiron:iron_ingot')
	end
	if minetest.settings:get_bool('spears_enable_steel_spear') then
		spears_register_spear('steel', S('Steel Spear'), 6, 35, 'default:steel_ingot')
	end
	if minetest.settings:get_bool('spears_enable_copper_spear') then
		spears_register_spear('copper', S('Copper Spear'), 4.8, 30, 'default:copper_ingot')
	end
	if minetest.settings:get_bool('spears_enable_bronze_spear') then
		spears_register_spear('bronze', S('Bronze Spear'), 5.5, 35, 'default:bronze_ingot')
	end
else
	if minetest.settings:get_bool('spears_enable_steel_spear') then
		spears_register_spear('steel', S('Steel Spear'), 6, 30, 'default:steel_ingot')
	end
	if minetest.settings:get_bool('spears_enable_copper_spear') then
		spears_register_spear('copper', S('Copper Spear'), 5, 30, 'default:copper_ingot')
	end
	if minetest.settings:get_bool('spears_enable_bronze_spear') then
		spears_register_spear('bronze', S('Bronze Spear'), 6, 35, 'default:bronze_ingot')
	end
end

if minetest.settings:get_bool('spears_enable_obsidian_spear') then
	spears_register_spear('obsidian', S('Obsidian Spear'), 8, 30, 'default:obsidian')
end

if minetest.settings:get_bool('spears_enable_diamond_spear') then
	spears_register_spear('diamond', S('Diamond Spear'), 8, 40, 'default:diamond')
end

if minetest.settings:get_bool('spears_enable_gold_spear') then
	spears_register_spear('gold', S('Golden Spear'), 5, 40, 'default:gold_ingot')
end
